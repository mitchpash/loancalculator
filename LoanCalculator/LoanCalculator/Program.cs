﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoanCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            //    When complete, uncommenting these lines should result in two tables being displayed to the console.

            Amortization table1 = new Amortization(new SerialLoan(10000, 0.02, 10));
            Amortization table2 = new Amortization(new AnnuityLoan(10000, 0.02, 10));

            table1.Print();
            table2.Print();

        }
    }
    public interface ILoan
    {
        //defind the interface for a Loan here
        double Principal { get; set; }
        double Rate { get; set; }
        int Periods { get; set; }

        double Payment(int i);
        double Outstanding(int i);
        double Interest(int i);
        double Repayment(int i);
    }
    public class AnnuityLoan : ILoan
    {
        //implement the interface here for an Annuity Loan
        public double Principal { get; set; }
        public double Rate { get; set; }
        public int Periods { get; set; }

        public AnnuityLoan(int i, double d, int i2)
        {
            Principal = i;
            Rate = d;
            Periods = i2;
        }
        public double Payment(int i)
        {
            return Principal * Rate / (1 - Math.Pow(1 + Rate, -Periods));
        }
        public double Outstanding(int n)
        {
            return Principal * Math.Pow(1 + Rate, n) - Payment(0) * (Math.Pow(1 + Rate, n) - 1) / Rate;
        }
        public double Interest(int n)
        {
            return Outstanding(n - 1) * Rate;
        }
        public double Repayment(int n)
        {
            return Payment(n) + Interest(n);
        }
    }
    public class SerialLoan : ILoan
    {
        //implement the interface here for a Serial Loan

        public double Principal { get; set; }
        public double Rate { get; set; }
        public int Periods { get; set; }

        public SerialLoan(int i, double d, int i2)
        {
            Principal = i;
            Rate = d;
            Periods = i2;
        }
        public double Payment(int i)
        {
            return Repayment(i) + Interest(i);
        }
        public double Outstanding(int n)
        {
            return Repayment(0) * (Periods - n);
        }
        public double Interest(int n)
        {
            return Outstanding(n - 1) * Rate;
        }
        public double Repayment(int n)
        {
            return Principal / Periods;
        }
    }
}
